Name:           mujs
Version:        1.2.0
Release:        3
Summary:        An embeddable Javascript interpreter
License:        ISC
URL:            http://mujs.com/
# The mujs.com download page doesn't provide recent tarballs
# Github mirror of mujs.com repository provides releases from tags
Source0:        https://mujs.com/downloads/mujs-%{version}.tar.gz

# CVE-2022-30974
Patch0001:      0001-Issue-162-Check-stack-overflow-during-regexp-compila.patch
Patch0002:      0002-Issue-161-Don-t-fclose-a-FILE-that-is-NULL.patch
# CVE-2022-30975
Patch0003:      0003-Issue-161-Cope-with-empty-programs-in-mujs-pp.patch
# CVE-2022-44789
Patch0004:      0004-Fix-use-after-free-in-getOwnPropertyDescriptor.patch

BuildRequires:  coreutils
BuildRequires:  gcc
BuildRequires:  grep
BuildRequires:  make
BuildRequires:  readline-devel

%description
MuJS is a lightweight Javascript interpreter designed for embedding in
other software to extend them with scripting capabilities.

%package devel
Summary:        MuJS development files
Provides:       %{name}-static = %{version}-%{release}

%description devel
This package provides the MuJS static library.

%prep
%setup -q -n %{name}-%{version}
chmod a-x -v docs/*

%patch0001 -p1
%patch0002 -p1
%patch0003 -p1
%patch0004 -p1

%build
make debug %{?_smp_mflags} XCFLAGS="%{optflags} -fPIC" LDFLAGS="%{?__global_ldflags}"

%install
make install DESTDIR=%{buildroot} prefix="%{_prefix}" libdir="%{_libdir}" \
 XCFLAGS="%{optflags} -fPIC" LDFLAGS="%{?__global_ldflags}"

%files
%license COPYING
%doc AUTHORS README docs
%{_bindir}/%{name}

%files devel
%license COPYING
%doc AUTHORS README
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/%{name}.h
%{_libdir}/lib%{name}.a

%changelog
* Wed Nov 30 2022 cenhuilin <cenhuilin@kylinos.cn> - 1.2.0-3
- fix: fix CVE-2022-44789

* Tue Sep 27 2022 liweiganga <liweiganga@uniontech.com> - 1.2.0-2
- fix: fix CVE-2022-30974 CVE-2022-30974

* Mon Sep 26 2022 liweiganga <liweiganga@uniontech.com> - 1.2.0-1
- upstream release 1.2.0
- fix: fix CVE-2016-10141、CVE-2016-7504、CVE-2016-9136、CVE-2017-5628、CVE-2016-9017、\
       CVE-2016-7506、CVE-2017-5627、CVE-2016-9108、CVE-2016-9294

* Thu May 13 2021 He Rengui <herengui@uniontech.com> - 1.0.9-1
- package init
